//This class is used for the statistics in the overview
export class Statistik{
    cardsCorrect:number;
    cardsOverall:number;
    averageCorrectness:number;
}